using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data;
using API.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class ImagesController : ControllerBase
  {
    private readonly DataContext _context;
    public ImagesController(DataContext context)
    {
      _context = context;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<Image>>> GetImages()
    {
        var images =  await _context.Images.ToListAsync();

        return Ok(images);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Image>> GetImage(int id)
    {
        var image = await _context.Images.FirstOrDefaultAsync(image => image.Id == id);
        return Ok(image);
    }
  }
}
using System;

namespace API.Entities
{
    public class Image
    {
        public int Id { get; set; }
        public string path { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string category { get; set; }
        public DateTime dateCreated { get; set; }
    }
}
